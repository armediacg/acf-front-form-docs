Quick Starter Guid
==================
.. warning:: Before creating any form, make sure you have **Advanced Custom Fields** installed and activated

In this starter guid, we'll create a form which creates a post with extra fields related to a book. These fields are :

- Book's ISBN
- Book's Author
- Date of publication

Creating The Fields
-------------------
First of all, we need to create those extra fields using Advanced Custom Fields (ACF), to do so, in the ACF menu click on **"Add New"**, this will create a new Fields Group which we'll call **"About the book"**.

Then, add the fields using the button **"Add Field"** :

===================== ===================== ==============
 Label                 Name                  Type        
===================== ===================== ==============
 ISBN                  isbn                  Text        
 Author                author                Text        
 Date of publication   date_of_publication   Date Picker 
===================== ===================== ==============

When you have done, click on **"Publish"**. Now the fields are ready to use.

.. image:: img/about-book.jpg

Adding the form into a page
---------------------------
Now we have a ACF Fields Group, we can add a form into any page in our website, let's create a page on Wordpress and call it **"Books Reviw"**.

- In this page, using **Visual Composer page builder** click on **Add Element**
- In the **Content** tab, select **ACF Front Form**, the ACF Front Form dialog appears

.. image:: img/vc.jpg

- In the **Post ID** field, set the value to :code:`new_post`, this means that each time a user submits the form, a new post will be created

.. image:: img/book/new_post.jpg

- Now, in the **Form** tab, find the **Submit Value** and set it to :code:`Submit`, this is the text displayed on the submit button of the Form

.. image:: img/book/submit.jpg

- You can now Save the settings and preview your page

.. image:: img/book/form.jpg

Add a specific form
-------------------
In some situations, you can have multiple Fields Groups of ACF, in this case we need to tell ACF Front Form to load only our specific Fiels Group.
To do so, go to ACF Front Form settings of your page, in the **Fields** tab, in the **Field Groups** field, check the the group you want to use.

.. image:: img/book/fields.jpg

.. tip:: If you want to add the title and content fields into the Form, then check the two options **Show the title** and **Show the content**

Save changes and that's it !

.. image:: img/book/form_title.jpg
