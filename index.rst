.. ACF Front Form Documentation documentation master file, created by
   sphinx-quickstart on Tue Jan 30 23:02:04 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ACF Front Form's documentation
=========================================
.. image:: src/img/title.jpg
By : Mourad Arifi
Email : arifi.armedia@gmail.com

Make Front end Forms with Advanced Custom Fields using WPBakery Page Builder (formerly Visual Composer) or shortcode without extra coding or hooking to use :code:`acf_form()` function, just add a shortcode with all needed paramerters.

.. raw:: html

    <iframe width="720" height="420" src="https://www.youtube.com/embed/YRBVhizrQLM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    

Guide
^^^^^
.. toctree::
   :maxdepth: 2

   src/installation
   src/quick-starter-guide
   src/add-form
   src/settings
   src/licence


Reviews

^^^^^^^

.. image:: src/img/support.jpg

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

